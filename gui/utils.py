from __future__ import annotations

from typing import List, Optional
import os
import h5py


def generateInputs(
    newFlat: str,
    oldFlat: str,
    energy: float,
    pyfaiConfig: dict,
    datasetFilename: str,
    scanNumber: int,
    monitorName: str,
    referenceCounts: int,
    detectorName: str,
    outputDirectory: str,
) -> List[dict]:
    """
    Generate input parameters for the EWOKS workflow.
    """
    baseDirName = os.path.splitext(os.path.basename(datasetFilename))[0]
    outputFilePathH5 = os.path.join(outputDirectory, f"{baseDirName}.h5")
    outputAsciiFileTemplate = os.path.join(outputDirectory, f"{baseDirName}_%04d.xye")

    inputs = [
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "newflat",
            "value": newFlat,
        },
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "oldflat",
            "value": oldFlat,
        },
        {
            "task_identifier": "FlatFieldFromEnergy",
            "name": "energy",
            "value": energy,
        },
        {
            "task_identifier": "PyFaiConfig",
            "name": "filename",
            "value": pyfaiConfig,
        },
        {
            "task_identifier": "PyFaiConfig",
            "name": "integration_options",
            "value": {"method": "no_csr_ocl_gpu"},
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "filename",
            "value": datasetFilename,
        },
        {"task_identifier": "IntegrateBlissScan", "name": "scan", "value": scanNumber},
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "output_filename",
            "value": outputFilePathH5,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "monitor_name",
            "value": monitorName,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "reference",
            "value": referenceCounts,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "maximum_persistent_workers",
            "value": 1,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "retry_timeout",
            "value": 3600,
        },
        {
            "task_identifier": "IntegrateBlissScan",
            "name": "detector_name",
            "value": detectorName,
        },
        {
            "task_identifier": "SaveNexusPatternsAsAscii",
            "name": "output_filename_template",
            "value": outputAsciiFileTemplate,
        },
        {
            "task_identifier": "SaveNexusPatternsAsAscii",
            "name": "enabled",
            "value": False,
        },
    ]
    outputPathJson = os.path.join(
        outputDirectory, f"{baseDirName}_{scanNumber}_{detectorName}.json"
    )

    return {
        "inputs": inputs,
        "convert_destination": outputPathJson,
    }


def extractScanNumber(h5path: str) -> int:
    """
    Extracts the scan number from the h5path from a selected node.

    Example: '/2.1/measurement/p3" -> returns 2'
    """
    parts = h5path.split("/")
    if len(parts) > 1:
        scanNumber = int(parts[1].split(".")[0])
        return scanNumber
    return -1


def generateUniquePath(basePath: str) -> str:
    """
    Generates a unique path by adding an incremantal suffix if necessary.

    Args:
        basePath: Base path (file or directory)

    Returns:
        unique path
    """
    isFile = os.path.isfile(basePath)
    dirName = os.path.dirname(basePath)
    baseName, ext = (
        os.path.splitext(os.path.basename(basePath)) if isFile else (basePath, "")
    )

    parts = baseName.rsplit("_", 1)
    if len(parts) == 2 and parts[1].isdigit():
        baseName, counter = parts[0], int(parts[1]) + 1
    else:
        counter = 1

    generatedName = os.path.join(dirName, f"{baseName}_{counter}{ext}")
    while os.path.exists(generatedName):
        counter += 1
        generatedName = os.path.join(dirName, f"{baseName}_{counter}{ext}")
    return generatedName


def getScanEnergy(h5Filename: str, scanNumber: int) -> Optional[float]:
    """
    Retrieves the energy for a specific scan in an HDF5 file.

    Args:
        h5Filename: Path to the HDF5 file.
        scanNumber: Scan number to retrieve energy for.

    Returns:
        Energy value for a given scan.
    """
    scanPath = f"{scanNumber}.1/instrument/positioners"

    with h5py.File(h5Filename, "r") as h5file:
        if scanPath in h5file and "energy" in h5file[scanPath].keys():
            return h5file[scanPath]["energy"][()]

    return None
