from __future__ import annotations

import argparse
import os
import logging
import threading

# Environment variable HDF5_USE_FILE_LOCKING must be set *before* importing HDF5 libraries
# This prevents file locking issues, especially in shared filesystems (e.g., NFS).
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"

from hdf5widget import Hdf5TreeView  # noqa
from utils import (  # noqa
    generateInputs,
    extractScanNumber,
    generateUniquePath,
    getScanEnergy,
)
from ewoks.bindings import execute_graph  # noqa
from ewoks.bindings import submit_graph  # noqa

import PyQt5.QtCore  # noqa
from silx.gui import qt, icons  # noqa
from silx.gui.utils.concurrent import submitToQtMainThread  # noqa

_logger = logging.getLogger(__name__)
NEWFLAT = "/home/spitoni/data/id31/inhouse/P3/flats.mat"
OLDFLAT = "/home/spitoni/data/id31/inhouse/P3/flats_old.mat"


class Id31FAIEwoksMainWindow(qt.QMainWindow):
    """
    This GUI is designed for reprocessing HDF5 scans data with fast azimuthal
    integration using Ewoks workflows (ewoksXRPD)
    """

    _SETTINGS_VERSION_STR = "1"

    def __init__(self, parent: qt.QWidget | None = None):
        super().__init__(parent)
        self.setWindowTitle("pyFAI with EWOKS - ID31")
        self.resize(1000, 800)

        self.statusBar = qt.QStatusBar()
        self.setStatusBar(self.statusBar)

        # Set paths
        self._defaultDirectoryRaw = ""
        self._workflow = os.path.join(
            os.path.dirname(__file__),
            "../workflows/integrate_with_saving_with_flat.json",
        )

        # Central Layout setup
        centralWidget = qt.QWidget(self)
        self.setCentralWidget(centralWidget)
        mainLayout = qt.QVBoxLayout(centralWidget)

        # HDF5 Viewer
        self._hdf5Widget = Hdf5TreeView()
        mainLayout.addWidget(self._hdf5Widget)

        # Menu and Toolbar setup
        self._setupMenuAndToolBar()

        # Config JSON Section
        configFileGroupBox = qt.QGroupBox("pyFAI config")
        configFileLayout = qt.QHBoxLayout()
        self._configFileLineEdit = qt.QLineEdit()
        self._configFileLineEdit.setPlaceholderText("/path/to/pyfai_config.json")

        loadConfigFileButton = qt.QPushButton("Open...")
        loadConfigFileButton.setIcon(icons.getQIcon("document-open"))
        loadConfigFileButton.clicked.connect(self._loadConfigFileButtonClicked)

        configFileLayout.addWidget(self._configFileLineEdit)
        configFileLayout.addWidget(loadConfigFileButton)

        configFileGroupBox.setLayout(configFileLayout)
        mainLayout.addWidget(configFileGroupBox)

        # Output Directory Section
        outputGroupBox = qt.QGroupBox("Output directory")
        outputLayout = qt.QHBoxLayout()
        self._outputLineEdit = qt.QLineEdit()
        self._outputLineEdit.setPlaceholderText("/path/to/output/directory")

        outputDirButton = qt.QPushButton("Select...")
        outputDirButton.setToolTip("Define a new output directory.")
        outputDirButton.setIcon(icons.getQIcon("folder"))
        outputDirButton.clicked.connect(self._outputDirButtonClicked)

        outputLayout.addWidget(self._outputLineEdit)
        outputLayout.addWidget(outputDirButton)

        outputGroupBox.setLayout(outputLayout)
        mainLayout.addWidget(outputGroupBox)

        # Process Button
        processButton = qt.QPushButton("Execute")
        processButton.setIcon(icons.getQIcon("next"))
        processButton.clicked.connect(self._executeButtonClicked)
        processButton.setToolTip(
            "Execute workflow locally, save HDF5 processed and Ewoks worflow (Json). "
        )
        mainLayout.addWidget(processButton)

        # Submit Button
        submitButton = qt.QPushButton("Submit")
        submitButton.setIcon(icons.getQIcon("next"))
        submitButton.clicked.connect(self._submitButtonClicked)
        submitButton.setToolTip(
            "Submit workflow to Ewoks worker remotely, save HDF5 processed and Ewoks worflow (Json). "
        )
        mainLayout.addWidget(submitButton)

    def _showHelpDialogClicked(self):
        """
        Display the Help dialog when the Help button is clicked.
        """
        helpDialog = qt.QMessageBox(self)
        helpDialog.setWindowTitle("Help")
        helpDialog.setIcon(qt.QMessageBox.Information)
        helpDialog.setText(
            "Welcome to the pyFAI with EWOKS application help."
        )  # TODO: Logo EWOKS
        helpDialog.setInformativeText(
            "How to get started?\n\n"
            "- Load raw data files (.h5).\n"
            "- Load the pyFAI configuration file.\n"
            "- Select an output directory to save the processed data.\n"
            "- Click Process to execute the workflow."
        )
        helpDialog.setStandardButtons(qt.QMessageBox.Ok)
        helpDialog.exec_()

    def _setupMenuAndToolBar(
        self,
    ) -> None:
        """
        Setup of the main menu and toolbar with actions for file handling.
        """
        menuBar = self.menuBar()
        fileMenu = menuBar.addMenu("File")

        openAction = qt.QAction(icons.getQIcon("document-open"), "Open...", self)
        openAction.triggered.connect(self._openActionTriggered)
        openAction.setShortcut(qt.QKeySequence.StandardKey.Open)
        reloadAction = qt.QAction(icons.getQIcon("view-refresh"), "Reload", self)
        reloadAction.triggered.connect(self._hdf5Widget.reloadSelected)
        reloadAction.setShortcut(qt.QKeySequence.StandardKey.Refresh)
        clearAction = qt.QAction(icons.getQIcon("remove"), "Clear", self)
        clearAction.triggered.connect(self._hdf5Widget.clearFiles)
        clearAction.setShortcut(qt.QKeySequence.StandardKey.Delete)

        fileMenu.addActions([openAction, reloadAction, clearAction])

        toolBar = self.addToolBar("File Actions")
        toolBar.addActions([openAction, reloadAction, clearAction])
        toolBar.setMovable(False)
        self.setContextMenuPolicy(qt.Qt.PreventContextMenu)
        toolBar.setFloatable(False)

        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Preferred)
        toolBar.addWidget(spacer)

        helpAction = qt.QAction(
            qt.QIcon.fromTheme("help-about"), "?", self
        )  # How TODO add an icon that is not existing in silx icons ???
        font = helpAction.font()
        font.setBold(True)
        helpAction.setFont(font)
        helpAction.setToolTip("About this app")
        helpAction.triggered.connect(self._showHelpDialogClicked)
        toolBar.addAction(helpAction)

    def addRawDataFile(self, fileName: str) -> None:
        """
        Proxy method to add a new file to the HDF5 tree viewer.
        """
        if not fileName or not os.path.isfile(fileName):
            qt.QMessageBox.warning(
                self, "Invalid File Format", "The selected file does not exist."
            )
            return
        if not fileName.endswith(".h5"):
            qt.QMessageBox.warning(
                self, "Invalid File Format", "Please select a valid HDF5 file (.h5)."
            )
            return
        self._hdf5Widget.addFile(fileName)
        self._defaultDirectoryRaw = os.path.dirname(fileName)

    def _openActionTriggered(self) -> None:
        """
        Add Raw data as HDF5 file without cleaning the tree viewer.
        """
        fileName, _ = qt.QFileDialog.getOpenFileName(
            self,
            "Add RAW data file",
            self._defaultDirectoryRaw,
            "H5 files (*.h5);;All files (*)",
        )
        if fileName:
            self.addRawDataFile(fileName)

    def _loadConfigFileButtonClicked(self) -> None:
        """
        Choose and import JSON or PONI config file.
        """
        currentDir = os.path.dirname(self.getConfigFilePath()) or os.getcwd()
        filePath, _ = qt.QFileDialog.getOpenFileName(
            self,
            "Open config file",
            currentDir,
            "JSON or PONI files (*.json *.poni);;All files (*)",
        )
        if filePath:
            self.setConfigFilePath(filePath)
        else:
            self.setConfigFilePath("")
            _logger.info("No config file chosen.")

    def getConfigFilePath(self) -> str:
        """
        Returns the current configuration file path from the line edit.
        """
        return self._configFileLineEdit.text().strip()

    def setConfigFilePath(self, path: str) -> None:
        """
        Update the configuration file path in the line edit.
        """
        self._configFileLineEdit.setText(path)

    def _getOutputDirName(self) -> str:
        """
        Returns the current output directory path from the line edit.
        """
        return self._outputLineEdit.text().strip()

    def _setOutputDirName(self, path: str) -> None:
        """
        Update the output directory path in the line edit.
        """
        self._outputLineEdit.setText(path)

    def _outputDirButtonClicked(self) -> None:
        """
        Choose an output directory using a dialog and set it in the line edit.
        """
        currentPath = self._getOutputDirName()
        if currentPath and os.path.exists(currentPath):
            pass
        else:
            currentPath = os.getcwd()
            _logger.info(f"Path not found. Falling back to: {currentPath}")

        newPath = qt.QFileDialog.getExistingDirectory(
            self, "Choose output directory", currentPath
        )
        if newPath:
            self._setOutputDirName(newPath)

    def _getParameters(self, datasetFilename: str, scanNumber: int) -> dict | None:
        """
        Generates parameters to execute workflow for a given scan.

        Args:
            datasetFilename: Filename of the HDF5 file containing the scan
            scanNumber: Number of the scan.

        Returns:
            Dictionnary of parameters for the workflow, or None if failed.
        """
        detectorName = "p3"
        monitorName = "scaled_mondio"

        energy = getScanEnergy(datasetFilename, scanNumber)

        if energy is not None:
            _logger.info(f"Energy for scan {scanNumber}: {energy}")
        else:
            _logger.error(
                f"Unable to retrieve energy for scan {scanNumber} in file {datasetFilename}."
            )
            return None

        outputDirectory = self._getOutputDirName()

        inputParameters = generateInputs(
            newFlat=NEWFLAT,
            oldFlat=OLDFLAT,
            energy=energy,
            pyfaiConfig=self.getConfigFilePath(),
            datasetFilename=datasetFilename,
            scanNumber=scanNumber,
            monitorName=monitorName,
            referenceCounts=1,
            detectorName=detectorName,
            outputDirectory=outputDirectory,
        )
        scanName = f"{os.path.basename(datasetFilename)}::{scanNumber}"
        inputParameters["scanName"] = scanName

        return inputParameters

    def _processScans(self, scans, local: bool = True) -> None:
        """
        Execute workflow and save HDF5 data and JSON workflow ewoks file from a single selected scan.

        Executing in a separate thread.
        """
        inputParameters = self._prepareScans(scans)

        if local:
            self._updateStatusBar("Processing scans locally...")
            thread = threading.Thread(
                target=self._executeWorkflowForParams, args=(inputParameters,)
            )
            thread.start()
        else:
            self._updateStatusBar("Submitting scans to worker...")
            self._submitWorkflowForParams(inputParameters)

    def _prepareScans(self, scans) -> list[dict]:
        """
        Prepares parameters for each selected scan.
        """
        uniqueScans = set()
        for scan in scans:
            scanNumber = extractScanNumber(scan.physical_name)
            if scanNumber == -1:
                _logger.warning(
                    f"Skipping scan: Invalid scan number in {scan.physical_name}"
                )
                continue
            uniqueScans.add((scan.physical_filename, scanNumber))

        inputParameters = list()
        for h5Filename, scanNumber in uniqueScans:
            workflowParameters = self._getParameters(h5Filename, scanNumber)
            if not workflowParameters:
                _logger.warning(
                    f"Skipping scan {h5Filename}::{scanNumber} due to missing parameters."
                )
                continue

            inputParameters.append(workflowParameters)

        return inputParameters

    def _showErrorInStatusBar(self, message: str) -> None:
        """
        Display an error message in the status bar and log it.

        Args:
            message: The error message to display.
        """
        _logger.error(message)
        submitToQtMainThread(self._updateStatusBar, message, error=True)

    def _executeWorkflowForParams(self, params) -> None:
        """
        Workflow execution for multiple scans in a single thread (locally).

        Args:
            params: List of workflow input parameters for each scan.
        """
        if not os.path.isfile(self._workflow):
            self._showErrorInStatusBar(f"Workflow file not found: {self._workflow}")
            return

        for workflowParameters in params:
            scanName = workflowParameters.pop("scanName", "Unknown scan")
            _logger.info(f"Processing workflow for parameters: {scanName}")
            try:
                execute_graph(self._workflow, **workflowParameters)
                _logger.info(f"Successfully processed scan: {scanName}")
            except Exception as e:
                errorMsg = f"Error processing scan {scanName}: {e}"
                _logger.error(errorMsg)
                submitToQtMainThread(self._updateStatusBar, errorMsg, error=True)
                raise e

        submitToQtMainThread(
            self._updateStatusBar, "Processing completed successfully."
        )

    def _executeButtonClicked(self) -> None:
        """
        Handle the process button click. Validate inputs, adjust output directory and process scans.
        """
        if not self._validateInputParameters():
            return

        selectedScanNodes = list(self._hdf5Widget.getSelectedNodes())
        self._adjustOutputDirectory()
        self._processScans(selectedScanNodes, local=True)

    def _processFutures(self, futures: list[tuple]) -> None:
        """
        Process the futures in a separate thread.

        Args:
            futures: A list of tuples containing -> (future, scanName).
        """
        failed = list()
        for future, scanName in futures:
            try:
                future.get()
            except Exception as e:
                _logger.error(f"Workflow failed for {scanName}: {e}")
                failed.append(scanName)

        if failed:
            errorMsg = f"Workflows completed with errors for scans: {', '.join(failed)}"
            submitToQtMainThread(self._updateStatusBar, errorMsg, error=True)

        else:
            successMsg = "All workflows completed successfully."
            submitToQtMainThread(self._updateStatusBar, successMsg)

    def _submitWorkflowForParams(self, params) -> None:
        """
        Submit workflow for multiple scans to the Ewoks worker (remotely).

        Args:
            params: List of workflow input parameters for each scan.
        """
        futures = list()

        for workflowParameters in params:
            scanName = workflowParameters.pop("scanName", "Unknown scan")
            _logger.info(f"Submitting workflow for parameters: {scanName}")

            try:
                future = submit_graph(self._workflow, **workflowParameters)
                futures.append((future, scanName))
                _logger.info(f"Successfully submitted scan: {scanName}")
            except Exception as e:
                error_msg = f"Error submitting scan {scanName}: {e}"
                _logger.error(error_msg)
                self._updateStatusBar(error_msg, error=True)

        thread = threading.Thread(target=self._processFutures, args=(futures,))
        thread.start()

    def _submitButtonClicked(self) -> None:
        """
        Handle the submit button click. Validate inputs, adjust output directory and submit scans to the worker.
        """

        if not self._validateInputParameters():
            return
        selectedScanNodes = list(self._hdf5Widget.getSelectedNodes())
        self._adjustOutputDirectory()
        self._processScans(selectedScanNodes, local=False)

    def _getValidationErrors(
        self, rawDataLoaded: bool, selectedScanNodes: list, configFileLoaded: bool
    ) -> list[str]:
        """
        Generate a list of validation error messages based on the current state.

        Args:
            rawDataLoaded: Whether raw data is loaded.
            selectedScanNodes: List of selected scan nodes.
            configFileLoaded: Whether a configuration file is loaded.

        Returns:
            A list of error messages if validation fails, otherwise an empty list.
        """
        errors = list()
        if not rawDataLoaded:
            errors.append("❌ No raw data file loaded.")
        if not selectedScanNodes:
            errors.append("❌ No scan selected.")
        if not configFileLoaded:
            errors.append("❌ No pyFAI config file loaded.")
        return errors

    def _showWarningMessage(self, errorMessages: list[str]) -> None:
        """
        Show a warning message box if prerequisites for processing are not met.
        """
        warningMessageBox = qt.QMessageBox(self)
        warningMessageBox.setWindowTitle("Workflow cannot be excecuted")
        warningMessageBox.setIcon(qt.QMessageBox.Warning)
        warningMessageBox.setText("\n".join(errorMessages))
        warningMessageBox.exec_()

    def _validateInputParameters(self) -> bool:
        """
        Validates that all necessary inputs are present before starting processing.

        Returns:
            True if inputs are valid, False otherwise.
        """
        rawDataLoaded = not self._hdf5Widget.isEmpty()
        selectedScanNodes = self._hdf5Widget.getSelectedNodes()
        configFileLoaded = bool(self.getConfigFilePath())

        errorMessages = self._getValidationErrors(
            rawDataLoaded, selectedScanNodes, configFileLoaded
        )

        if errorMessages:
            self._updateStatusBar(" ".join(errorMessages), error=True)
            _logger.warning(f"Processing cannot start: {' '.join(errorMessages)}")
            self._showWarningMessage(errorMessages)
            return False

        for node in selectedScanNodes:
            _logger.info(
                f"Selected node for processing: {node.physical_filename}::{node.physical_name}"
            )
        return True

    def _adjustOutputDirectory(self) -> None:
        """
        Adjusts the output directory if it already exists.
        """
        currentOutputDir = self._getOutputDirName()
        if os.path.exists(currentOutputDir):
            newOutputDirectory = generateUniquePath(currentOutputDir)

            if not self._showConfirmationMessage(newOutputDirectory):
                _logger.info("Process canceled by the user.")
                return

            os.makedirs(newOutputDirectory, exist_ok=True)
            self._setOutputDirName(newOutputDirectory)
            _logger.info(f"New output directory created: {newOutputDirectory}")

        else:
            self._setOutputDirName(currentOutputDir)
            _logger.info(f"Output directory set to: {currentOutputDir}")

    def _showConfirmationMessage(self, newOutputDirectory: str) -> bool:
        """Show a confirmation message box when creating a new directory for output file.

        Returns True is the user confirms, otherwise False.
        """
        confirmationMessageBox = qt.QMessageBox(self)
        confirmationMessageBox.setWindowTitle("Output Directory Warning")
        confirmationMessageBox.setIcon(qt.QMessageBox.Warning)
        confirmationMessageBox.setText("The second output directory is not empty.")
        confirmationMessageBox.setInformativeText(
            f"A new directory will be created:\n{newOutputDirectory}"
        )
        confirmationMessageBox.setStandardButtons(
            qt.QMessageBox.Ok | qt.QMessageBox.Cancel
        )
        userResponse = confirmationMessageBox.exec_()
        return userResponse == qt.QMessageBox.Ok

    def _updateStatusBar(self, message: str, error: bool = False):
        """
        Updates the status bar with a message.

        If 'error' is True, displays the message in red.
        """
        self.statusBar.setStyleSheet("color: red;" if error else "")
        self.statusBar.showMessage(message)

    def loadSettings(self) -> None:
        """
        Load user settings.
        """
        settings = qt.QSettings()

        if settings.value("version") != self._SETTINGS_VERSION_STR:
            _logger.info("Setting version mismatch. Clearing settings.")
            settings.clear()
            return

        geometry = settings.value("mainWindow/geometry")
        if geometry:
            self.restoreGeometry(geometry)

        self._defaultDirectoryRaw = settings.value("parameters/inputDirectory", "")
        self.setConfigFilePath(settings.value("parameters/configFile", ""))
        outputDirectory = settings.value("parameters/outputDirectory", "")
        self._setOutputDirName(outputDirectory)

        _logger.info(
            "Settings loaded: Config file = %s, Output directory = %s",
            self.getConfigFilePath(),
            self._getOutputDirName(),
        )

    def closeEvent(self, event: qt.QCloseEvent) -> None:
        """
        Save user settings on close.
        """
        settings = qt.QSettings()

        settings.setValue("version", self._SETTINGS_VERSION_STR)
        settings.setValue("mainWindow/geometry", self.saveGeometry())
        settings.setValue("parameters/inputDirectory", self._defaultDirectoryRaw)
        settings.setValue("parameters/configFile", self.getConfigFilePath())
        settings.setValue("parameters/outputDirectory", self._getOutputDirName())

        event.accept()


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Perform azimuthal integration of selected scans"
    )
    parser.add_argument(
        "-f",
        "--fresh",
        action="store_true",
        help="Start without loading previous user preferences",
    )
    parser.add_argument(
        "-r",
        "--raw",
        type=str,
        default=None,
        help="Path to the raw data file (HDF5 format)",
    )
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=None,
        help="Path to the configuration file",
    )
    args = parser.parse_args()

    app = qt.QApplication([])
    app.setOrganizationName("ESRF")
    app.setOrganizationDomain("esrf.fr")
    app.setApplicationName("id31pyfaiewoks")

    window = Id31FAIEwoksMainWindow()
    window.setAttribute(qt.Qt.WA_DeleteOnClose)

    if not args.fresh:
        _logger.info(
            "Launching application in default mode. Loading previous settings."
        )
        window.loadSettings()
    if args.raw:
        raw_data = os.path.abspath(args.raw)
        if os.path.isfile(raw_data) and raw_data.endswith(".h5"):
            window.addRawDataFile(raw_data)
        else:
            _logger.error(f"Invalid raw data file path or format: {raw_data}")

    if args.config:
        config_file = os.path.abspath(args.config)
        if os.path.isfile(config_file):
            window.setConfigFilePath(config_file)
        else:
            _logger.error(f"Invalid config file path: {config_file}")

    window.show()
    app.exec_()
